package com.image.recogniser.services;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class FilesService {

    @Value("${package.to.save.files}")
    private String packageToSaveFiles;

    @Value("${package.to.save.master.images}")
    private String packageToSaveMasterImages;

    @Value("${package.to.save.client.images}")
    private String packageToSaveClientImages;

    @Value("${package.to.save.master.file}")
    private String packageToSaveMasterFile;

    @Value("${package.to.save.client.file}")
    private String packageToSaveClientFile;

    @Value("${min.match.percent.to.show.result}")
    private int minMatchPercentToShowResult;

    public ArrayList<String> parseCSV(String linkToFile) {

        ArrayList<String> links = new ArrayList<String>();

        CsvMapper mapper = new CsvMapper();
        // important: we need "array wrapping" (see next section) here:
        mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
        File csvFile = new File(linkToFile); // or from String, URL etc
        MappingIterator<String[]> it = null;
        try {
            it = mapper.readerFor(String[].class).readValues(csvFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        while (it.hasNext()) {
            String[] row = it.next();
            for (int i = 1; i < row.length; i++) {
                if (row[i].contains("http:/") || row[i].contains("https:/")) {
                    if(row[i].contains(".jpg") || row[i].contains(".png") || row[i].contains(".gif") || row[i].contains(".jpeg")){
                        links.add(row[i]);
                    }
                }
            }
        }

        return links;
    }

    public void downloadFilefromUrlToDirectory(String fileURL, String pathToSave){

        String extension = null;
        String fileName = null;
        URL url = null;

        try {
            String link = fileURL;

            url = new URL(link);

            BufferedImage image1 = ImageIO.read(url);

            extension = link.substring((link.lastIndexOf(".") + 1), link.length());

            fileName = link.substring((link.lastIndexOf("/") + 1), link.length());

            File folder = new File(pathToSave);

            if(!folder.exists())
            {
                folder.mkdirs();
            }

            File outputFile = new File(folder, fileName);

            ImageIO.write(image1, extension , outputFile);


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void inputStreamToFileAndSave(InputStream initialStream, String pathToSave, String fileName) throws Exception{

        File folder = new File(pathToSave);

        if(!folder.exists())
        {
            folder.mkdirs();
        }

        File targetFile = new File(folder, fileName);
        OutputStream outStream = new FileOutputStream(targetFile);


        IOUtils.copy(initialStream, outStream);
        initialStream.close();
        outStream.close();


    }

    public Boolean checkFilesInPackage(String packagePath) {
        File fMaster = new File(packagePath);
        File[] masterFiles = fMaster.listFiles();
        boolean masterFlag = false;
        if(masterFiles != null){
            masterFlag = masterFiles.length > 0;
        }
        return masterFlag ;
    }

    public Map cleanDirectory(String directory){
        String directoryWithFile = "";

        if(directory.contains("client")){
            directoryWithFile = packageToSaveClientFile;
        }else{
            directoryWithFile = packageToSaveMasterFile;
        }


        File directoryFile = new File(packageToSaveFiles + directoryWithFile);
        boolean deleteStatus = false;

        try {
            FileUtils.cleanDirectory(directoryFile);
            deleteStatus = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, Boolean> responseMap =  new HashMap<>();
        responseMap.put("delete", deleteStatus);
        return responseMap;
    }

    public Map uploadFile(Map statusMap, MultipartHttpServletRequest request){
        Map <String, String> responseMap =  new HashMap<>();

        String resultMassage = "";

        statusMap.put("downloadPercent", 0);
        statusMap.put("downloadStatus", "processing");

        Map<String, MultipartFile> stringMultipartFileMap = request.getFileMap();
        for(Map.Entry<String, MultipartFile> multipartFileEntry: stringMultipartFileMap.entrySet()){

            String packageToSaveFile = "";
            String packageImageName = "";

            if(multipartFileEntry.getValue().getName().contains("client")){
                packageToSaveFile = packageToSaveFiles + packageToSaveClientFile;
                packageImageName = packageToSaveClientImages;
            }else{
                packageToSaveFile = packageToSaveFiles + packageToSaveMasterFile;
                packageImageName = packageToSaveMasterImages;
            }

            String fileName = multipartFileEntry.getValue().getOriginalFilename();

            try {
                InputStream initialStream = multipartFileEntry.getValue().getInputStream();
                inputStreamToFileAndSave(initialStream, packageToSaveFile, fileName);
            } catch (Exception e) {
                resultMassage = "Error, no file";
                statusMap.put("downloadStatus", "stop");
                break;
            }
            ArrayList<String> linkList = parseCSV(packageToSaveFile + "/" + fileName);

            int allLinks = linkList.size();
            int count = 0;
            for(String link :linkList){
                downloadFilefromUrlToDirectory(link, packageToSaveFile + packageImageName);
                count++;

                Double percent = (count*100d/allLinks);
                statusMap.put("downloadPercent", percent.intValue());
            }
            resultMassage = "File Uploaded successfully.";
        }

        statusMap.put("downloadStatus", "stop");
        responseMap.put("status", resultMassage);
        return responseMap;
    }

    }

