package com.image.recogniser.services;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class CompareImagesService {

    @Value("${package.to.save.files}")
    private String packageToSaveFiles;

    @Value("${package.to.save.master.images}")
    private String packageToSaveMasterImages;

    @Value("${package.to.save.client.images}")
    private String packageToSaveClientImages;

    @Value("${package.to.save.master.file}")
    private String packageToSaveMasterFile;

    @Value("${package.to.save.client.file}")
    private String packageToSaveClientFile;

    @Value("${min.match.percent.to.show.result}")
    private int minMatchPercentToShowResult;

    ImageMatcherUtil imageMatcherUtil = new ImageMatcherUtil();

    DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public Map compareImages(ArrayList matchList, Map statusMap, int matchUserPercentBorder){
        matchList.clear();

        statusMap.put("compareProcessPercent", 0);
        statusMap.put("compareStatus", "processing");

        String responseStatus = "";

        File fMaster = new File(packageToSaveFiles + packageToSaveMasterFile + packageToSaveMasterImages);
        File[] masterFiles = fMaster.listFiles();

        File fClient= new File(packageToSaveFiles + packageToSaveClientFile + packageToSaveClientImages);
        File[] clientFiles = fClient.listFiles();

        int fullCompareTimes =  masterFiles.length * clientFiles.length;
        int count=0;

        try {

            if(masterFiles != null && clientFiles !=null ) {
                for (int i = 0; i < masterFiles.length; i++) {
                    for (int y = 0; y < clientFiles.length; y++) {

                        BufferedImage imageMaster = null;
                        BufferedImage imageClient = null;

                        imageMaster = ImageIO.read(masterFiles[i]);
                        imageClient = ImageIO.read(clientFiles[y]);

                        double matchPercent = imageMatcherUtil.matchImages(imageMaster, imageClient);

                        count++;
                        double percentResult = count*100d/fullCompareTimes;

                        int borderPercent = minMatchPercentToShowResult;

                        if(matchUserPercentBorder > 0 && matchUserPercentBorder < 100){
                            borderPercent = matchUserPercentBorder;
                        }

                        if(matchPercent > borderPercent){
                            MatchObject matchObject = new MatchObject();
                            matchObject.setImageMaster(masterFiles[i].getName());
                            matchObject.setImageClient(clientFiles[y].getName());
                            matchObject.setMatchPercent(matchPercent);

                            matchList.add(matchObject);
                        }

                        statusMap.put("compareProcessPercent", decimalFormat.format(percentResult));
                    }

                }
                responseStatus = "Comparison finished success";
            }

        } catch (IOException e) {
            e.printStackTrace();
            responseStatus = "Error IO";
        }


        statusMap.put("compareStatus", "stop");

        Map <String, Object> responseMap =  new HashMap<>();
        responseMap.put("status", responseStatus);
        responseMap.put("matchMap", matchList);
        return responseMap;
    }




    public Map compare2Images(MultipartHttpServletRequest request) throws IOException {

        MultipartFile file = request.getFile("masterImage");
        file.getInputStream();
        MultipartFile file2 = request.getFile("clientImage");
        file.getInputStream();
        BufferedImage image1 = ImageIO.read(file.getInputStream());
        BufferedImage image2 = ImageIO.read(file2.getInputStream());
        return imageMatcherUtil.match2imagesWithShift(image1, image2);
    }


    public BufferedImage resizeAndSetType(BufferedImage img, int newW, int newH, int imgtype ) {

        int w = img.getWidth();
        int h = img.getHeight();
        BufferedImage dimg = new BufferedImage(newW, newH,  imgtype);
        Graphics2D g = dimg.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(img, 0, 0, newW, newH, 0, 0, w, h, null);
        g.dispose();
        return dimg;
    }


    public   double compareImageInProcentage (BufferedImage img1, BufferedImage img2){
        int width1 = img1.getWidth(null);
        int width2 = img2.getWidth(null);
        int height1 = img1.getHeight(null);
        int height2 = img2.getHeight(null);
        if ((width1 != width2) || (height1 != height2)) {
            System.err.println("Error: Images dimensions mismatch");
            System.exit(1);
        }
        long diff = 0;
        for (int y = 0; y < height1; y++) {
            for (int x = 0; x < width1; x++) {
                int rgb1 = img1.getRGB(x, y);
                int rgb2 = img2.getRGB(x, y);
                if(((rgb1>>24) == 0x00)){
//                    img1.setRGB(x, y, new Color(255,255,255).getRGB());
                    rgb1 = new Color(255,255,255).getRGB();
                }
                if(((rgb2>>24) == 0x00)){
//                    img2.setRGB(x, y, new Color(255,255,255).getRGB());
                    rgb2 = new Color(255,255,255).getRGB();
                }

                int r1 = (rgb1 >> 16) & 0xff;
                int g1 = (rgb1 >>  8) & 0xff;
                int b1 = (rgb1      ) & 0xff;
                int r2 = (rgb2 >> 16) & 0xff;
                int g2 = (rgb2 >>  8) & 0xff;
                int b2 = (rgb2      ) & 0xff;
                diff += Math.abs(r1 - r2);
                diff += Math.abs(g1 - g2);
                diff += Math.abs(b1 - b2);

                if (rgb1 != rgb2) {
                    diff++;
                }
            }
        }

        double n = width1 * height1 * 3;
        double p = diff / n / 255.0;

        double result = p*100;

        return result;
    }
}
