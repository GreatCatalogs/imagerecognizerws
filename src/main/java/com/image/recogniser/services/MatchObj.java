package com.image.recogniser.services;

public class MatchObj {

    private double matchPercent = 0;

    private int xBestPosition;

    private int yBestPosition;

    private boolean masterFirst;

    private boolean clientFirst;

    public double getMatchPercent() {
        return matchPercent;
    }

    public void setMatchPercent(double matchPercent) {
        this.matchPercent = matchPercent;
    }

    public int getyBestPosition() {
        return yBestPosition;
    }

    public void setyBestPosition(int yBestPosition) {
        this.yBestPosition = yBestPosition;
    }

    public int getxBestPosition() {
        return xBestPosition;
    }

    public void setxBestPosition(int xBestPosition) {
        this.xBestPosition = xBestPosition;
    }

    public boolean isMasterFirst() {
        return masterFirst;
    }

    public void setMasterFirst(boolean masterFirst) {
        this.masterFirst = masterFirst;
    }

    public boolean isClientFirst() {
        return clientFirst;
    }

    public void setClientFirst(boolean clientFirst) {
        this.clientFirst = clientFirst;
    }
}
