package com.image.recogniser.services;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ImageMatcherUtil {

    public static final int IMG_WIDTH = 128;
    public static final int IMG_HEIGHT = 128;

    public static final int MAX_COMPARED_SHADE = 250;
    public static final double SHADE_MAX_DIFF = 0.07;

    private static final int xShift = 32;
    private static final int yShift = 32;

    private static MatchObj matchObj;

    public double matchImages(BufferedImage masterImage, BufferedImage clientImage) {

//        for (int y = 0; y < IMG_HEIGHT; y++) {
//            boolean masterOneColorRowFlag = true;
//            boolean clientOneColorRowFlag = true;
//
//            for (int x = 1; x < IMG_WIDTH; x++) {
//
//                int masterRGB1 = masterSmallImg.getRGB(x, y);
//                int masterRGB2 = masterSmallImg.getRGB(x - 1, y);
//                int clientRGB1 = clientSmallImg.getRGB(x, y);
//                int clientRGB2 = clientSmallImg.getRGB(x - 1, y);
//
//                int masterShade1 = (masterRGB1 >> 16) & 0xff;
//                int masterShade2 = (masterRGB2 >> 16) & 0xff;
//
//                int clientShade1 = (clientRGB1 >> 16) & 0xff;
//                int clientShade2 = (clientRGB2 >> 16) & 0xff;
//
//                if (clientOneColorRowFlag) {
//                    if (clientShade1 != 255) {
//                        if (!(clientShade1 < (clientShade2 + 5)) || !(clientShade1 > (clientShade2 - 5))) {
//                            clientOneColorRowFlag = false;
//                        }
//                    }
//                }
//
//                if (masterOneColorRowFlag) {
//                    if (masterShade1 != 255) {
//                        if (!(masterShade1 < (masterShade2 + 5)) || !(masterShade1 > (masterShade2 - 5))) {
//                            masterOneColorRowFlag = false;
//                        }
//                    }
//                }
//
//            }
//
//            if (masterOneColorRowFlag) {
//                for (int x = 0; x < IMG_WIDTH; x++) {
//                    masterSmallImg.setRGB(x, y, new Color(255, 255, 255).getRGB());
//                }
//            }
//            if (clientOneColorRowFlag) {
//                for (int x = 0; x < IMG_WIDTH; x++) {
//                    clientSmallImg.setRGB(x, y, new Color(255, 255, 255).getRGB());
//                }
//            }
//
//        }
//
//
//
//        for (int y = 0; y < IMG_HEIGHT; y++) {
//            boolean masterOneColorRowFlag = true;
//            boolean clientOneColorRowFlag = true;
//
//            for (int x = 1; x < IMG_WIDTH; x++) {
//
//                int masterRGB1 = masterSmallImg.getRGB(x, y);
//                int masterRGB2 = masterSmallImg.getRGB(x - 1, y);
//                int clientRGB1 = clientSmallImg.getRGB(x, y);
//                int clientRGB2 = clientSmallImg.getRGB(x - 1, y);
//
//                int masterShade1 = (masterRGB1 >> 16) & 0xff;
//                int masterShade2 = (masterRGB2 >> 16) & 0xff;
//
//                int clientShade1 = (clientRGB1 >> 16) & 0xff;
//                int clientShade2 = (clientRGB2 >> 16) & 0xff;
//
//                if (clientOneColorRowFlag) {
//                    if (clientShade1 != 255) {
//                        if (!(clientShade1 < (clientShade2 + 5)) || !(clientShade1 > (clientShade2 - 5))) {
//                            clientOneColorRowFlag = false;
//                        }
//                    }
//                }
//
//                if (masterOneColorRowFlag) {
//                    if (masterShade1 != 255) {
//                        if (!(masterShade1 < (masterShade2 + 5)) || !(masterShade1 > (masterShade2 - 5))) {
//                            masterOneColorRowFlag = false;
//                        }
//                    }
//                }
//
//            }
//
//            if (masterOneColorRowFlag) {
//                for (int x = 0; x < IMG_WIDTH; x++) {
//                    masterSmallImg.setRGB(x, y, new Color(255, 255, 255).getRGB());
//                }
//            }
//            if (clientOneColorRowFlag) {
//                for (int x = 0; x < IMG_WIDTH; x++) {
//                    clientSmallImg.setRGB(x, y, new Color(255, 255, 255).getRGB());
//                }
//            }
//
//        }



//        for (int y = 0; y < masterSmallImg.getHeight(); y++) {
//            for (int x = 0; x < masterSmallImg.getWidth(); x++) {
//                int masterRGB1 = masterSmallImg.getRGB(x, y);
//                int masterShade1 = (masterRGB1 >> 16) & 0xff;
//                System.out.print(masterShade1 + " ");
//            }
//            System.out.println();
//        }



//        File outputfile = new File("/home/okalmykov/Work/imageTest/bwImage.jpg");
//        try {
//            ImageIO.write(masterSmallImg, "jpg", outputfile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }



        for (int y = 0; y < masterImage.getHeight(); y++) {
            for (int x = 0; x < masterImage.getWidth(); x++) {
                int pixel = masterImage.getRGB(x, y);
                boolean isTransparent = ((pixel>>24) == 0x00);
                if (isTransparent) {
                    //set white color instead of transparent
                    masterImage.setRGB(x, y, new Color(255,255,255).getRGB());
                }
            }
        }


        for (int y = 0; y < clientImage.getHeight(); y++) {
            for (int x = 0; x < clientImage.getWidth(); x++) {
                int pixel = clientImage.getRGB(x, y);
                boolean isTransparent = ((pixel>>24) == 0x00);
                if (isTransparent) {
                    //set white color instead of transparent
                    clientImage.setRGB(x, y, new Color(255,255,255).getRGB());
                }
            }
        }

        BufferedImage masterSmallImg = resizeImage(masterImage, BufferedImage.TYPE_BYTE_GRAY);
        BufferedImage clientSmallImg = resizeImage(clientImage, BufferedImage.TYPE_BYTE_GRAY);

        int totalPixels = IMG_HEIGHT * IMG_WIDTH;
        int matchedPixels = 0;

        for (int y = 0; y < IMG_HEIGHT; y++) {
            for (int x = 0; x < IMG_WIDTH; x++) {

                int masterRGB = masterSmallImg.getRGB(x, y);
                int clientRGB = clientSmallImg.getRGB(x, y);

                int masterShade = (masterRGB >> 16) & 0xff;
                int clientShade = (clientRGB >> 16) & 0xff;

                if (clientShade > MAX_COMPARED_SHADE || masterShade > MAX_COMPARED_SHADE) {
                    totalPixels--;
                    continue;
                }

                double shadeDiff = ((double) Math.abs(masterShade - clientShade) / 255.0);

                if (shadeDiff < SHADE_MAX_DIFF) {
                    matchedPixels++;
                }
            }
        }

        return ((double) matchedPixels / (double) totalPixels) * 100;
    }



    public Map match2imagesWithShift(BufferedImage masterImage, BufferedImage clientImage){

        matchObj = new MatchObj();

        int masterWidth = masterImage.getWidth();
        int masterHeight = masterImage.getHeight();

        for (int y = 0; y < masterHeight; y++) {
            for (int x = 0; x < masterWidth; x++) {
                int pixel = masterImage.getRGB(x, y);
                boolean isTransparent = ((pixel>>24) == 0x00);
                if (isTransparent) {
                    //set white color instead of transparent
                    masterImage.setRGB(x, y, new Color(255,255,255).getRGB());
                }
            }
        }

        int clientWidth = clientImage.getWidth();
        int clientHeight = clientImage.getHeight();

        for (int y = 0; y < clientHeight; y++) {
            for (int x = 0; x < clientWidth; x++) {
                int pixel = clientImage.getRGB(x, y);
                boolean isTransparent = ((pixel>>24) == 0x00);
                if (isTransparent) {
                    //set white color instead of transparent
                    clientImage.setRGB(x, y, new Color(255,255,255).getRGB());
                }
            }
        }

        BufferedImage masterSmallImg = resizeImage(masterImage, BufferedImage.TYPE_BYTE_GRAY);
        BufferedImage clientSmallImg = resizeImage(clientImage, BufferedImage.TYPE_BYTE_GRAY);

        //move image over other, client over master
        for(int i = 0; i <= xShift; i++ ){
            for(int j = 0; j <= yShift; j++){
                compareImagesUseShift(masterSmallImg, clientSmallImg, i, j, matchObj, true);

            }
        }

        //move image over other, master over client
        for(int i = 0; i <= xShift; i++ ){
            for(int j = 0; j <= yShift; j++){
                compareImagesUseShift(clientSmallImg, masterSmallImg, i, j, matchObj, false);

            }
        }

//        System.out.println("Max match obj  " + matchObj.getMatchPercent() + " x- " + matchObj.getxBestPosition() + " y- " +matchObj.getyBestPosition());

        Map responseMap = new HashMap();
        responseMap.put("matchPixelPercent", matchObj.getMatchPercent());
        responseMap.put("matchHistogramPercent", compare2Histogram(masterImage, clientImage));


        try {
            if(matchObj.isMasterFirst()){
                responseMap.put("resultMatchImage",  paintResultMatchImage(masterSmallImg, clientSmallImg, matchObj.getxBestPosition(), matchObj.getyBestPosition()));
            }else{
                responseMap.put("resultMatchImage",  paintResultMatchImage(clientSmallImg, masterSmallImg, matchObj.getxBestPosition(), matchObj.getyBestPosition()));

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        return responseMap;
    }



    private static double compareImagesUseShift(BufferedImage masterSmallImg, BufferedImage clientSmallImg, int xShift, int yShift, MatchObj matchObj, boolean masetFileFirst){

        int totalPixels = 0;
        int matchedPixels = 0;

        s:for (int y = 0; y < IMG_HEIGHT; y++) {
            for (int x = 0; x < IMG_WIDTH; x++) {


                int masterRGB = 0;

                try {
                    masterRGB = masterSmallImg.getRGB(x + xShift, y + yShift);
                }catch(Exception e){
                    continue s;
                }
                int clientRGB = 0;
                try {
                    clientRGB = clientSmallImg.getRGB(x, y);
                }catch (Exception e){
                    continue s;
                }

                int masterShade = (masterRGB >> 16) & 0xff;
                int clientShade = (clientRGB >> 16) & 0xff;


                if (clientShade > MAX_COMPARED_SHADE || masterShade > MAX_COMPARED_SHADE) {
                    continue;
                }else{
                    totalPixels++;
                }

                double shadeDiff = ((double) Math.abs(masterShade - clientShade) / 255.0);

                if (shadeDiff < SHADE_MAX_DIFF) {
                    matchedPixels++;
                }
            }
        }


        double localmaxMatch =(((double) matchedPixels / (double) totalPixels) * 100);

        if(matchObj.getMatchPercent() < localmaxMatch) {
            matchObj.setMatchPercent(localmaxMatch);
            matchObj.setxBestPosition(xShift);
            matchObj.setyBestPosition(yShift);
            matchObj.setMasterFirst(masetFileFirst);
        }

        return localmaxMatch;

    }

    //Compare 2 images and save result image in webapp/matchImage/ package
    public Map match2image(BufferedImage masterImage, BufferedImage clientImage, double clientMacShadeDiff) throws IOException {

        int masterWidth = masterImage.getWidth();
        int masterHeight = masterImage.getHeight();

        for (int y = 0; y < masterHeight; y++) {
            for (int x = 0; x < masterWidth; x++) {
                int pixel = masterImage.getRGB(x, y);
                boolean isTransparent = ((pixel>>24) == 0x00);
                if (isTransparent) {
                    //set white color instead of transparent
                    masterImage.setRGB(x, y, new Color(255,255,255).getRGB());
                }
            }
        }
        int clientWidth = clientImage.getWidth();
        int clientHeight = clientImage.getHeight();

        for (int y = 0; y < clientHeight; y++) {
            for (int x = 0; x < clientWidth; x++) {
                int pixel = clientImage.getRGB(x, y);
                boolean isTransparent = ((pixel>>24) == 0x00);
                if (isTransparent) {
                    //set white color instead of transparent
                    clientImage.setRGB(x, y, new Color(255,255,255).getRGB());
                }
            }
        }


        BufferedImage masterSmallImg = resizeImage(masterImage, BufferedImage.TYPE_BYTE_GRAY);
        BufferedImage clientSmallImg = resizeImage(clientImage, BufferedImage.TYPE_BYTE_GRAY);


        BufferedImage matchedSmallImg = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics2D matchedImgGraphics = matchedSmallImg.createGraphics();
        matchedImgGraphics.drawImage(clientSmallImg, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        matchedImgGraphics.dispose();


        int totalPixels = IMG_HEIGHT * IMG_WIDTH;
        int matchedPixels = 0;

        for (int y = 0; y < IMG_HEIGHT; y++) {
            for (int x = 0; x < IMG_WIDTH; x++) {

                int masterRGB = masterSmallImg.getRGB(x, y);
                int clientRGB = clientSmallImg.getRGB(x, y);

                int masterShade = (masterRGB >> 16) & 0xff;
                int clientShade = (clientRGB >> 16) & 0xff;

                //blue not used for comparation as it is background
                if (clientShade > MAX_COMPARED_SHADE || masterShade > MAX_COMPARED_SHADE) {
                    matchedSmallImg.setRGB(x, y, new Color(0,0,255).getRGB());
                    totalPixels--;
                    continue;
                }

                double shadeDiff = ((double) Math.abs(masterShade - clientShade) / 255.0);

                //green is matching
                double shadeMaxDifParam = SHADE_MAX_DIFF;

                if(clientMacShadeDiff != 0){
                    shadeMaxDifParam = clientMacShadeDiff/100d;
                }

                if (shadeDiff < shadeMaxDifParam) {
                    matchedPixels++;
                    matchedSmallImg.setRGB(x, y, new Color(0,255,0).getRGB());
                } else {
                    //red is not matching
                    matchedSmallImg.setRGB(x, y, new Color(255,0,0).getRGB());
                }
            }
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(matchedSmallImg, "jpeg", baos);
        byte[] bytes = baos.toByteArray();


        double matchPercent = ((double) matchedPixels / (double) totalPixels) * 100;

        Map responseMap = new HashMap();
        responseMap.put("matchPercent", matchPercent);
        responseMap.put("resultMatchImageName", bytes);

        return responseMap;
    }


    public byte[] paintResultMatchImage ( BufferedImage masterSmallImg, BufferedImage clientSmallImg, int xShiftLoc, int yShiftLoc ) throws IOException {

        BufferedImage matchedSmallImg = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics2D matchedImgGraphics = matchedSmallImg.createGraphics();
        matchedImgGraphics.drawImage(clientSmallImg, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        matchedImgGraphics.dispose();

         for (int y = 0; y < IMG_HEIGHT; y++) {
             s: for (int x = 0; x < IMG_WIDTH; x++) {


                int masterRGB = 0;

                try {
                    masterRGB = masterSmallImg.getRGB(x + xShiftLoc, y + yShiftLoc);
                }catch(Exception e){
                    matchedSmallImg.setRGB(x, y, new Color(0,0,255).getRGB());
                    continue s;
                }
                int clientRGB = 0;
                try {
                    clientRGB = clientSmallImg.getRGB(x, y);
                }catch (Exception e) {
                    matchedSmallImg.setRGB(x, y, new Color(0,0,255).getRGB());
                    continue s;
                }
//
//                int masterRGB = masterSmallImg.getRGB(x, y);
//                int clientRGB = clientSmallImg.getRGB(x, y);

                int masterShade = (masterRGB >> 16) & 0xff;
                int clientShade = (clientRGB >> 16) & 0xff;

                //blue not used for comparation as it is background
                if (clientShade > MAX_COMPARED_SHADE || masterShade > MAX_COMPARED_SHADE) {
                    matchedSmallImg.setRGB(x, y, new Color(0,0,255).getRGB());
                    continue;
                }

                double shadeDiff = ((double) Math.abs(masterShade - clientShade) / 255.0);

                //green is matching
                if (shadeDiff < SHADE_MAX_DIFF) {
                    matchedSmallImg.setRGB(x, y, new Color(0,255,0).getRGB());
                } else {
                    //red is not matching
                    matchedSmallImg.setRGB(x, y, new Color(255,0,0).getRGB());
                }
            }
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(matchedSmallImg, "jpeg", baos);

        return baos.toByteArray();
    }

    public double compare2Histogram(BufferedImage masterImage, BufferedImage clientImage){

        ArrayList<double[]> arrMaster = imageHistogram(masterImage);
        ArrayList<double[]> arrClient= imageHistogram(clientImage);

        int mismatch  = 0;

        for(int i=0; i < arrMaster.size(); i++){
            for(int y=0; y < arrMaster.get(i).length; y++){
                double[] colorMaster  = arrMaster.get(i);
                double shadeMaster = colorMaster[y];

                double[] colorClient  = arrClient.get(i);
                double shadeClient = colorClient[y];

                double difference = Math.abs( shadeMaster - shadeClient);
                if(difference > 0.001){
                    mismatch++;
                }
            }
        }
        return (100d - (mismatch/768d)*100);
    }


    public ArrayList<double[]> imageHistogram(BufferedImage input) {

        int[] rhistogram = new int[256];
        int[] ghistogram = new int[256];
        int[] bhistogram = new int[256];

        for(int i=0; i<rhistogram.length; i++) rhistogram[i] = 0;
        for(int i=0; i<ghistogram.length; i++) ghistogram[i] = 0;
        for(int i=0; i<bhistogram.length; i++) bhistogram[i] = 0;

        for(int i=0; i<input.getWidth(); i++) {
            for(int j=0; j<input.getHeight(); j++) {

                int red = new Color(input.getRGB (i, j)).getRed();
                int green = new Color(input.getRGB (i, j)).getGreen();
                int blue = new Color(input.getRGB (i, j)).getBlue();

                // Increase the values of colors
                rhistogram[red]++; ghistogram[green]++; bhistogram[blue]++;

            }
        }

        ArrayList<int[]> hist = new ArrayList<int[]>();
        //all histogram
        hist.add(rhistogram);
        hist.add(ghistogram);
        hist.add(bhistogram);


        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        ArrayList<double[]> histPr = new ArrayList<double[]>();

        double allPixel = input.getWidth()*input.getHeight();

        double[] rhistogramPr = new double[256];
        double[] ghistogramPr = new double[256];
        double[] bhistogramPr = new double[256];
        for(int i=0; i < rhistogram.length; i++){
            rhistogramPr[i] = rhistogram[i]/allPixel;
            ghistogramPr[i] = ghistogram[i]/allPixel;
            bhistogramPr[i] = bhistogram[i]/allPixel;
        }

        // percent of each color in histogram
        histPr.add(rhistogramPr);
        histPr.add(ghistogramPr);
        histPr.add(bhistogramPr);

        return histPr;
    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int type) {
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();
        return resizedImage;
    }
}
