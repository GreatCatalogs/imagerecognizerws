package com.image.recogniser.services;

public class MatchObject {

    private String imageMaster;
    private String imageClient;
    private double matchPercent;

    public String getImageMaster() {
        return imageMaster;
    }

    public void setImageMaster(String imageMaster) {
        this.imageMaster = imageMaster;
    }

    public double getMatchPercent() {
        return matchPercent;
    }

    public void setMatchPercent(double matchPercent) {
        this.matchPercent = matchPercent;
    }

    public String getImageClient() {
        return imageClient;
    }

    public void setImageClient(String imageClient) {
        this.imageClient = imageClient;
    }
}
