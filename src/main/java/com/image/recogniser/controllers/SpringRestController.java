package com.image.recogniser.controllers;

import com.image.recogniser.services.CompareImagesService;
import com.image.recogniser.services.FilesService;
import com.image.recogniser.services.MatchObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@RestController
public class SpringRestController {

    @Autowired
    FilesService filesService;

    @Autowired
    CompareImagesService compareImagesService;

    @Value("${package.to.save.files}")
    private String packageToSaveFiles;

    @Value("${package.to.save.master.images}")
    private String packageToSaveMasterImages;

    @Value("${package.to.save.client.images}")
    private String packageToSaveClientImages;

    @Value("${package.to.save.master.file}")
    private String packageToSaveMasterFile;

    @Value("${package.to.save.client.file}")
    private String packageToSaveClientFile;

    @Value("${min.match.percent.to.show.result}")
    private int minMatchPercentToShowResult;

    ConcurrentHashMap statusMap = new ConcurrentHashMap();

    ArrayList<MatchObject> matchList = new ArrayList<>();


    @RequestMapping(value = "/save-info-file", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public Object fileUpload(MultipartHttpServletRequest request) {

        return filesService.uploadFile(statusMap, request);
    }

    @RequestMapping(value = "/check-upload-process", method = RequestMethod.GET)
    public Object checkUploadProcess(HttpServletRequest request) {
        return statusMap;
    }

    @RequestMapping(value = "/delete-file", method = RequestMethod.GET)
    public Object cleanDirectory(@RequestParam(value = "directory") String directory) {

        return filesService.cleanDirectory(directory);
    }

    @RequestMapping(value = "/check-master-client-file", method = RequestMethod.GET)
    public Object checkMasterClientFiles(HttpServletRequest request) throws Exception {

        boolean masterFlag = filesService.checkFilesInPackage(packageToSaveFiles + packageToSaveMasterFile);

        boolean clientFlag = filesService.checkFilesInPackage(packageToSaveFiles + packageToSaveClientFile);

        Map <String, Boolean> responseMap =  new HashMap<>();
        responseMap.put("masterFilePresent", masterFlag);
        responseMap.put("clientFilePresent", clientFlag);
        return responseMap;
    }

    @RequestMapping(value = "/compare-images", method = RequestMethod.GET)
    public Object compareImagesFromMasterClientDirectory(@RequestParam(value = "matchPercent") Integer matchPercent) {

        return compareImagesService.compareImages(matchList, statusMap, matchPercent);
    }

    @RequestMapping(value = "/check-compare-process", method = RequestMethod.GET)
    public Object checkCompareProcess(HttpServletRequest request) {
        return statusMap;
    }

    @RequestMapping(value = "/check-match-list", method = RequestMethod.GET)
    public Object checkMatchList(HttpServletRequest request) {
        Map <String, Object> responseMap =  new HashMap<>();
        responseMap.put("matchList", matchList);
        return responseMap;
    }

    @RequestMapping(value = "/compare-two-images", method = RequestMethod.POST, consumes = "multipart/form-data")
    public Object compareTwoImages(MultipartHttpServletRequest request) throws Exception {

        Map matchPercent = compareImagesService.compare2Images(request);

//        Map <String, String> responseMap =  new HashMap<>();
//        responseMap.put("matchPercentage", Double.toString(matchPercent));
        return matchPercent;

    }
}