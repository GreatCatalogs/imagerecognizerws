$(function() {

    $("#uploadClientForm").submit(function(e){
        var formData = new FormData(this);

        $(document.body).css({ 'cursor': 'wait' });
        $('.app-button').prop('disabled', true);
        $.ajax({
            url: "ws/save-info-file",
            type: 'POST',
            data:  formData,
            dataType : 'json',
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                $(document.body).css({ 'cursor': 'default' });
                console.log(data.status);
                $('.app-button').prop('disabled', false);
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                $(document.body).css({ 'cursor': 'default' });
                console.log('error');
                $('.app-button').prop('disabled', false);
            }
        });
        e.preventDefault();

        setTimeout(checkUploadProcess, 2000)
    });


    $("#uploadMasterForm").submit(function(e){
        var formData = new FormData(this);
        $(document.body).css({ 'cursor': 'wait' });
        $('.app-button').prop('disabled', true);
        $.ajax({
            url: "ws/save-info-file",
            type: 'POST',
            data:  formData,
            dataType : 'json',
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                $(document.body).css({ 'cursor': 'default' });
                console.log(data.status);
                $('.app-button').prop('disabled', false);
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                $(document.body).css({ 'cursor': 'default' });
                console.log('error');
                $('.app-button').prop('disabled', false);

            }
        });
        e.preventDefault();
        setTimeout(checkUploadProcess, 2000)
    });

    $("#multiform").submit(function(e){
        $("#itemPreview").hide();
        $("#resultImage").hide();
        $(document.body).css({ 'cursor': 'wait' });
        var formData = new FormData(this);
        $.ajax({
            url: "ws/compare-two-images",
            type: 'POST',
            data:  formData,
            dataType : 'json',
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                $(document.body).css({ 'cursor': 'default' });
                $("#resultCompare2Images").html("Match pixel percent:" + data.matchPixelPercent + "% <br> " + "Match histogram percent:" + data.matchHistogramPercent + "%");

                $("#resultImage").show();
                $("#itemPreview").show();
                $("#itemPreview").attr("src", "data:image/jpeg;base64," + data.resultMatchImage);
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                $(document.body).css({ 'cursor': 'default' });
                console.log('error');
            }
        });
        e.preventDefault();
    });

    checkMasterClientOnServer();

});

function compareImages(){

    $("#resultTable").html("");

    $(document.body).css({ 'cursor': 'wait' });

    $.get( "ws/compare-images?matchPercent="+ $("#userMatchPercent").val(), function(data, status){
        $(document.body).css({ 'cursor': 'default' });
    });

    setTimeout(checkCompareProcess, 1000);
}

function checkMasterClientOnServer(){

    $.get( "ws/check-master-client-file", function(data, status){
        if(data){
            if(data["masterFilePresent"] == true  && data["clientFilePresent"] == true){
                $("#startComparison").prop('disabled', false);
            }else{
                $("#startComparison").prop('disabled', true);
            }

            if(data["masterFilePresent"] == true){
                $("#deleteMasterFileButton").show();
                $("#uploadMasterFileButton").hide();
            }else{
                $("#deleteMasterFileButton").hide();
                $("#uploadMasterFileButton").show();
            }
            if(data["clientFilePresent"] == true){
                $("#deleteClientFileButton").show();
                $("#uploadClientFileButton").hide();
            }else{
                $("#deleteClientFileButton").hide();
                $("#uploadClientFileButton").show();
            }
        }
    });


}

function deleteFileOnWS(directory){
    $.get( "ws/delete-file?directory=" + directory, function(data, status){
        if(data){
            console.log("File deleted");
            checkMasterClientOnServer();
        }
    });
}

function checkUploadProcess(){
    $.get( "ws/check-upload-process", function(data, status){
        if(data){
            var uploadEl = $("#uploadResult");
            uploadEl.show();
            uploadEl.html(data.downloadPercent+"%");

            var progressEl = $( "#progressbar" );
            progressEl.val(data.downloadPercent);
            progressEl.show();

            if(data.downloadStatus == "processing") {
                setTimeout(checkUploadProcess, 2000)
            }else{
                $('.app-button').prop('disabled', false);
                $(document.body).css({ 'cursor': 'default' });
                uploadEl.hide();
                progressEl.hide();
                checkMasterClientOnServer();
            }
        }
    });
}

function checkCompareProcess(){
    $.get( "ws/check-compare-process", function(data, status){
        if(data){

            $('.app-button').prop('disabled', true);

            var uploadEl = $("#uploadResult");
            uploadEl.show();
            uploadEl.html(data.compareProcessPercent +"%");

            var progressEl = $( "#progressbar" );
            progressEl.val(data.compareProcessPercent);
            progressEl.show();

            if(data.compareStatus == "processing") {
                setTimeout(checkCompareProcess, 3000)
                checkMatchResults();
            }else {
                $(document.body).css({ 'cursor': 'default' });
                uploadEl.hide();
                progressEl.hide();
                $('.app-button').prop('disabled', false);

            }
        }
    });
}

var privLength = 0;

function checkMatchResults(){
    $.get( "ws/check-match-list", function(data, status){

        for(var i = privLength; i < data.matchList.length; i++){

            $("#resultTable").append( "<tr><td>"+data.matchList[i].matchPercent +"</td><td>" + data.matchList[i].imageMaster +"</td><td>" + data.matchList[i].imageClient +"</td></tr>" );
        }
        privLength = data.matchList.length;
    });
}
