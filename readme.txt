1) In properties file  "/imagerecogniserws/src/main/resources/app.properties" add path to package where app save
files with link and images.
 Example: "package.to.save.files = /opt/image-recognizer/"

 In pom.xml add path with deploy directory war
  <jboss.deploy.dir>..../apache-tomcat-8.0.30/webapps</jboss.deploy.dir>


2)To compare images from to files (master and client) first of all you need to save this to files on server

3)URL to save master or client .csv file with link to images
POST
http://<hostname>/imageRecogniserWS/ws/save-info-file

Request examples:

MASTER file save on server:
html:

<h2>Upload Master File</h2>
<form name="uploadMasterForm" id="uploadMasterForm" method="POST" enctype="multipart/form-data">

 <!--name of input for MASTER file have to be  - name="masterFile"-->
    <input id="masterFile" type="file" name="masterFile">
    <input class="app-button" id="uploadMasterFileButton" type="submit" value="Upload">
</form>

js (JQuery):
    $("#uploadClientForm").submit(function(e){
        var formData = new FormData(this);

        $.ajax({
            url: "http://<hostname>/ws/save-info-file",
            type: 'POST',
            data:  formData,
            dataType : 'json',
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {


In response you can get JSON, if success :{ status="File Uploaded successfully."}, if failed : { status="Error IO"}

            },
            error: function(jqXHR, textStatus, errorThrown)
            {

            }
        });
        e.preventDefault();

    });

CLIENT file save on server:
html:

<h2>Upload Client File</h2>
<form name="uploadClientForm" id="uploadClientForm" method="POST" enctype="multipart/form-data">

<!--name of input for CLIENT file have to be  - name="clientFile"-->

    <input id="clientFile" type="file" name="clientFile">
    <input class="app-button" id="uploadClientFileButton" type="submit" value="Upload">
</form>

js (JQuery) (the same as for master file):
    $("#uploadClientForm").submit(function(e){
        var formData = new FormData(this);

        $.ajax({
            url: "http://<hostname>/ws/save-info-file",
            type: 'POST',
            data:  formData,
            dataType : 'json',
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {

In response you can get JSON, if succes :{ status="File Uploaded successfully."}, if failed : { status="Error IO"}

            },
            error: function(jqXHR, textStatus, errorThrown)
            {

            }
        });
        e.preventDefault();

    });

4)This ("http://<hostname>/imageRecogniserWS/ws/save-info-file") request can take several minutes as during this
 request app is downloading images by link from this files
To check process of downloading images on server use:

URL to check downloading process:
GET
http://<hostname>/ws/check-upload-process


 in response you get JSON  data = { downloadPercent=1,  downloadStatus="processing"}

        while file is processing - downloadStatus="processing"
        when it finished - downloadStatus="stop"

Example:

function checkUploadProcess(){
    $.get( "http://<hostname>/ws/check-upload-process", function(data, status){

        if(data){

            if(data.downloadStatus == "processing") {


                setTimeout(checkUploadProcess, 2000)
            }else{

            }
        }
    });
}

5) To check if MASTER an CLIENT files on server use URL:
GET
http://<hostname>/ws/check-master-client-file

In response you get JSON data = { clientFilePresent=true,  masterFilePresent=false}

Example:
js (JQuery):

 function checkUploadProcess(){
     $.get( "http://<hostname>/ws/check-master-client-file", function(data, status){

     });
 }

 6) To delete MASTER or CLIENT file and all images from this file from server use URL:
 GET
 http://<hostname>/ws/delete-file?directory=<directoryName>
 directoryName = 'client' (CLIENT file) or 'master' (MASTER file)

 Example:
 function deleteFileOnWS(directory){
     $.get( "http://<hostname>/ws/delete-file?directory=" + directory, function(data, status){

     });
 }


7) When all files and images on server you can START comparison, use URL:

 GET
 http://<hostname>/ws/compare-images?matchPercent=50

 matchPercent - low bored of match percent to show in results

 In response you get JSON data = { matchMap=[
                                                 { imageMaster="D548-130_KO.gif",  imageClient="D548-130_KO.gif",  matchPercent=100},
                                                 { imageMaster="630XX21_KO.gif",  imageClient="50404-38-sw.jpg",  matchPercent=85.49127640036731}
                                             ],
                                   status="Comparison finished success"}

 Example:
 $.get( "http://<hostname>/ws/compare-images?matchPercent="+ $("#userMatchPercent").val(), function(data, status){

     });

8) Comparison process can take from 1 minutes to several hour, it depends from number of images, so to check
this process URL:

GET
http://<hostname>/ws/check-upload-process

In response you get JSON data = { compareProcessPercent="19.27",
                                  compareStatus="processing"
                                  }
 compareStatus= "processing" or "stop"

Example:
function checkUploadProcess(){
    $.get( "http://<hostname>/ws/check-upload-process", function(data, status){

    });
}

9) To check match results during comparison process or in the END of this process use URL:
GET
http://<hostname>/ws/check-match-list

In response you get JSON: data: { matchList=[
                                        { imageMaster="2020008_KO.gif",  imageClient="D548-130_KO.gif",  matchPercent=59.128630705394194}
                                        { imageMaster="2020008_KO.gif",  imageClient="exhilaration-chocolate-reclining-loveseat.jpg",  matchPercent=53.01891027439538}
                                       ]
                           }

Example:
function checkMatchResults(){
    $.get( "http://<hostname>/ws/check-match-list", function(data, status){

    });
}


10)To compare two images use URL

POST
http://<hostname>/ws/compare-two-image

In response you get JSON data :  { matchPixelPercent=24.546807951659517,  matchHistogramPercent=32.235920535257953, resultMatchImage=////image byte array////}

to show image in UI example:

HTML:
 <img id="itemPreview" src="" />

JS (JQuery)
$("#itemPreview").attr("src", "data:image/jpeg;base64," + data.resultMatchImage);


Request example:
HTML:
<h2>Compare 2 images</h2>
<form name="multiform" id="multiform" method="POST" enctype="multipart/form-data">
    Master image :<input type="file" name="masterImage" /><br/>
    Client image :<input type="file" name="clientImage" /><br/>
    <input type="submit"value="Upload">
</form>

name="masterImage" - required input name for master image
name="clientImage" - required input name for client image

JS (JQuery):
 $("#multiform").submit(function(e){
        var formData = new FormData(this);
        $.ajax({
            url: "ws/compare-two-images",
            type: 'POST',
            data:  formData,
            dataType : 'json',
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {

            },
            error: function(jqXHR, textStatus, errorThrown)
            {

            }
        });
        e.preventDefault();
    }
 );



/////////

Server:
1)Download and Install a Java Development Kit.
2)Download and Install apache-tomcat-8.0.30 from http://tomcat.apache.org/download-80.cgi
3)Start Up Tomcat:
 $CATALINA_HOME/bin/startup.sh (Unix)
4)Shut Down Tomcat:
 $CATALINA_HOME/bin/shutdown.sh (Unix)

5) Put app .war file in .../apache-tomcat-8.0.30/webapps directory








